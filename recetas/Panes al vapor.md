## Ingredientes

| cantidad | ingrediente               | porcentaje |
|----------|---------------------------|------------|
| 305g     | agua                      | 58.1       |
| 45g      | leche en polvo descremada | 8.57       |
| 8.75g    | levadura instantanea      | 1.67       |
| 470g     | harina                    | 89.52      |
| 85g      | azucar                    | 16.19      |
| 55g      | fecula de papa            | 10.48      |
| 20g      | aceite de canola          | 3.81       |
| 10g      | sal fina                  | 1.9        |

## Pasos

1. Disolver la leche y levadura en el agua
2. Agregar el resto de los ingredientes
3. Mezclar hasta tener desarrollo de gluten completo
4. Levar 45m, sin pliegues
5. Dividir, preformar, formar
6. Levado final, 20-40min dependiendo la temperatura, poner sobre cuadraditos de
   papel de manteca
7. Cocinar en vaporera, con el papel de manteca, por 20min


## Notas
Use maizena en vez de fecula de papa porque no tenía.
Para bollitos o para mantou, preformar en batard de masomenos 30min y cortar en
pedazos de masomenos 4cm de ancho, a mi me quedaron 7 pancitos, pero deberia
haberlos hecho mas chicos por el tamaño de la vaporera.

Comer en seguida, calentitos. Si los vas a comer despues, recalentar en vaporera
por 2 minutos.

## Credito

Receta de Modernist Bread, volumen 5, página 237 (119 del pdf)