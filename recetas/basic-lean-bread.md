# Ingredients

| ingredient | amount | % |
|--|--|--|
| water | 260g | 65 |
| IDY | 2g | 0.5 |
| bread flour | 400g | 100 |
| salt | 8g| 2 |


## Day 1
1. Measure water in a big bowl. Measure the yeast and add it to the water, stir to dissolve.
2. Mix flour and salt in a separate bowl, and add it to the bowl with water.
3. Mix well with a wooden spoon or your hand, or both, until no bits of dry flour remain anywhere in the bowl. Cover and let rest for 20 minutes.
4. Knead or use the [rubaud method](https://www.youtube.com/watch?v=zgz0oAhgwyg) depending on how wet it is.
5. After getting a uniform round dough, cover and let it rest at room temperature until about doubled in size. How long this takes will depend on your room temperature, should be a few hours.
6. Dust your counter or table with flour and remove the dough from the bowl. Shape into a tight ball. You can look at the Trevor Wilson above for some cues on technique or also [this video](https://www.youtube.com/watch?v=MPdedk9gJLQ)
7. Let it rest on the counter for 10-20 minutes seam side down. Meanwhile, flour a banneton or a soup bowl with a kitchen towel, and place the loaf seam side up in it. Cover or place it inside a plastic bag and into the fridge for 12 to 24h.


## Day 2
1. Preheat the oven with the dutch oven inside, to 446f/230c for about 40 minutes. Prepare your lame/razor and be mindful of what kitchen towels and/or gloves you'll use to handle the hot dutch oven.
2. Remove the dutch oven from the oven (always keeping in mind to close the oven door quickly so as to not let heat escape), and now, if you have a regular, deep dutch oven, place the dough, seam side down on the counter, you can put it on parchment paper to make it easier to transfer. Score the loaf and place it (again, seam side down) into the dutch oven. 
3. Cover the dutch oven and place it in the oven, set a timer for 20 minutes and go take a well deserved rest.
4. After 20 minutes it's time to uncover, remove the lid, being careful of the steam that will be released, and leave the bread uncovered in the oven for 15 to 35 more minutes, depending on the size of the loaf and the desired color. A deep dark brown is not burnt, it'll give extra flavor. Place the dutch oven with the bread on top of an upside down baking sheet to prevent the bottom of the loaf from burning.
5. When it's done remove from the oven and place the loaf on a wire rack right way. Let it cool until room temperature, usually at least 2 hours.


## Notes
If you have a combo cooker or similar you can do the scoring afte placing the loaf in the pan, and when you uncover you should set up the pot you were using as lid with the mouth facing up and place the pan with the bread on top to prevent the bottom from burning.