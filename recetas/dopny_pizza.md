# Ingredientes 

| ingrediente          | porcentaje | gramos |
|----------------------|------------|--------|
| Harina               | 100%       | 622g   |
| Agua                 | 61%        | 379g   |
| Levadura instantanea | 0.5%       | 3.1g   |
| Sal                  | 1.75%      | 11g    |
| Azucar               | 2%         | 12.5g  |
| Aceite               | 3%         | 18g    |
| Malta                | 0.25%      | 1.5g   |

Da 4 bollos de 260g

- Mezclar levadura en el agua, combinar la harina, sal, azucar y malta.
- Harina en el agua, mezclar un poco, incorporar aceite.
- Dejar descanzar 5min y amasar 3 minutos. No vas a necesitar mucha harina para
  amasar
- Dividir en bollos, darle forma (uso esta tecnica https://www.youtube.com/watch?v=ckxfSacDbzg&t=313 )
- Poner los bollos en tappers aceitados y a la heladera por 2 dias.
- Sacar de la heladera 3-5 horas antes de la coccion.

- Precalentar la piedra por 1 hora al maximo.
- Estirar la pizza, ponerla sobre pala harinada.
- Tratar de poner la salsa y el queso rapido, sacudir un poco para que no se
  pegue.
- Al horno, rotandola un par de veces cada unos minutos.
- Sacarla cuando este dorada la masa y el queso derretido.
- Dejar enfriar 5 minutos para que el queso no sea lava.

Consejos mios:

De las harinas que probe quede mas conforme con la campodonico pan dulce, el
levado de 48h con azucar y malta hace que requiera una harina bastante fuerte.

Si tenes horno que tiene el cajon abajo de la llama, una vez que esta dorada de
abajo (con puntos casi quemados), sacar y pasar abajo, yo lo hago con la misma
reja que uso para dejar enfriar la pizza/pan, puesta sobre una pizzera, y la
pizza sobre la reja, para que no se humedezca y ablande mientras se termina de
dorar de arriba. En 1 o 2 minutos se deberia terminar de dorar.


Depende que tan humeda sea la muzza que uses, por ahi vas a tener que cocinar la
masa con la salsa primero porque sino se va a separar mucho el aceite del queso.
A mi me pasa esto con la barraza, pero no con la punta de agua. Generalmente te
das cuenta que tan humeda es la muzza si la podes rallar facil. La barraza me es
imposible rallarla, la punta de agua es facil.

La receta es del usuario dopny de reddit:
https://www.reddit.com/r/Pizza/wiki/recipe/dough
